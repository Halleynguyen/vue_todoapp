// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'

Vue.config.productionTip = false

  let app;
  // Initialize Firebase
  let config = {
    apiKey: "AIzaSyBZERxsCEs3xy07tnwOi1lK5Em7GJtrjpE",
    authDomain: "vuetodoapp.firebaseapp.com",
    databaseURL: "https://vuetodoapp.firebaseio.com",
    projectId: "vuetodoapp",
    storageBucket: "vuetodoapp.appspot.com",
    messagingSenderId: "793245851898"
  };
  firebase.initializeApp(config)
  firebase.auth().onAuthStateChanged(function(user) {
    if (!app) {
      app = new Vue({
        el: '#app',
        template: '<App/>',
         components: { App },
        router
      })
    }
  });
